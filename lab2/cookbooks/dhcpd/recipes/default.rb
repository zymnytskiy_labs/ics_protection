#
# Cookbook:: dhcpd
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise 'Unsupported platform' if node['platform'] != 'ubuntu'

include_recipe '::install'
include_recipe '::config'
