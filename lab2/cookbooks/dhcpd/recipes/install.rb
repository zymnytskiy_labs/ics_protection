#
# Cookbook:: dhcpd
# Recipe:: install
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise 'Unsupported platform' if node['platform'] != 'ubuntu'

package 'isc-dhcp-server'
