#
# Cookbook:: dhcpd
# Recipe:: config
#
# Copyright:: 2017, The Authors, All Rights Reserved.

def cidr_to_netmask(cidr)
  require 'ipaddr'
  IPAddr.new('255.255.255.255').mask(cidr)
end

raise 'Unsupported platform' if node['platform'] != 'ubuntu'

if node['dhcpd']['interfaces'].nil?
  raise "node['dhcpd']['interfaces'] is required"
end

raise "node['dhcpd']['gateway'] is required" if node['dhcpd']['gateway'].nil?

template '/etc/default/isc-dhcp-server' do
  source 'isc-dhcp-server.erb'
  variables interfaces: node['dhcpd']['interfaces'].keys.join(' ')
  notifies :restart, 'service[isc-dhcp-server]', :delayed
end

service 'isc-dhcp-server' do
  action :enable
  supports restart: true
end

chef_gem 'netaddr'

subnets = []
node['dhcpd']['interfaces'].each do |interface, config|
  require 'netaddr'

  netaddr = NetAddr::CIDR.create config['netaddr']
  subnet = {}
  subnet[:interface] = interface
  subnet[:netaddr] = netaddr
  subnet[:vars] = {}
  subnet[:vars][:net_addr] = netaddr.network
  subnet[:vars][:net_mask] = cidr_to_netmask(netaddr.netmask[1..-1].to_i)
  subnet[:vars][:int_addr] = netaddr.nth(1).to_s
  subnet[:vars][:br_addr] = netaddr.broadcast
  subnet[:vars][:ranges] = [[netaddr.nth(10).to_s, netaddr.nth(200).to_s]]
  subnet[:vars][:boot_file] = config['boot_file']
  subnet[:vars][:next_server] = config['next_server']

  subnets.push subnet
end

subnets.each do |subnet|
  ifconfig subnet[:vars][:int_addr] do
    device subnet[:interface]
    onboot 'yes'
    notifies :restart, 'service[isc-dhcp-server]', :delayed
  end
end

template '/etc/dhcp/dhcpd.conf' do
  source 'dhcpd.conf.erb'
  notifies :restart, 'service[isc-dhcp-server]', :delayed
  variables(
    subnets: subnets.map { |subnet| subnet[:vars] },
    gateway: node['dhcpd']['gateway']
  )
end
