#
# Cookbook:: tftpd
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise 'unsupported platform' if node[:platform] != 'ubuntu'

package 'tftpd-hpa'

service 'tftpd-hpa' do
  action [:enable, :start]
end

directory node['tftpd']['root'] do
  owner 'root'
  group 'nogroup'
  mode '755'
end

template '/etc/default/tftpd-hpa' do
  source 'tftpd-hpa.erb'
  mode '644'
  variables tftp_root: node['tftpd']['root']
  notifies :restart, 'service[tftpd-hpa]'
end
