#
# Cookbook:: grub
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise 'Unsupported platform' if node['platform'] != 'ubuntu' && node['platform'] != 'centos'

template '/etc/grub.d/10_linux' do
  hash = `echo '123\n123' | grub-mkpasswd-pbkdf2 | awk 'END { print($NF); }'`

  source '10_linux.erb'
  mode '0700'
  variables hash: hash
end

execute 'update grub.cfg' do
  command 'grub-mkconfig > /boot/grub/grub.cfg'
end

file '/boot/grub/grub.cfg' do
  mode '0600'
end
