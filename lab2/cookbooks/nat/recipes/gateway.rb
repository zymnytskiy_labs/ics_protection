#
# Cookbook:: nat
# Recipe:: gateway
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise "node['nat']['external'] is required" if node['nat']['external'].nil?

include_recipe 'iptables'
include_recipe '::ip_forward'

iptables_rule "masquerade_#{node['nat']['external']}" do
  lines [
    '*nat',
    "-A POSTROUTING -o #{node['nat']['external']} -j MASQUERADE"
  ].flatten.join("\n")
end
