#
# Cookbook:: squid
# Recipe:: install
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise 'unsupported platform' if node['platform'] != 'ubuntu'

package 'squid'
