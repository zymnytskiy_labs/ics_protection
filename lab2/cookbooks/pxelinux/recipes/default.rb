#
# Cookbook:: pxelinux
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

raise 'unsupported platform' if node[:platform] != 'ubuntu'

include_recipe 'tftpd'

package 'syslinux'
package 'pxelinux'

tftp_root = node['tftpd']['root']

remote_file 'copy pxelinux.0' do
  path "#{tftp_root}/pxelinux.0"
  source 'file:///usr/lib/PXELINUX/pxelinux.0'
  owner 'root'
  group 'nogroup'
  mode '0644'
end

['ldlinux.c32', 'libutil.c32', 'menu.c32'].each do |filename|
  remote_file "copy #{filename}" do
    path "#{tftp_root}/#{filename}"
    source "file:///usr/lib/syslinux/modules/bios/#{filename}"
    owner 'root'
    group 'nogroup'
    mode '0644'
  end
end

directory "#{tftp_root}/pxelinux.cfg" do
  owner 'root'
  group 'nogroup'
  mode '0755'
end

template "#{tftp_root}/pxelinux.cfg/default" do
  source 'default.erb'
end
