CREATE ROLE guest LOGIN;

DROP VIEW IF EXISTS books_info;
CREATE VIEW books_info AS
    SELECT b.id, b.title, b.year, b.annotation, a.first_name || ' ' || a.last_name AS author
    FROM books AS b
    JOIN authors AS a on b.author_id = a.id;

GRANT SELECT ON books_info TO guest;

