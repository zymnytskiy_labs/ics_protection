CREATE SEQUENCE IF NOT EXISTS books_id_seq;

CREATE TABLE IF NOT EXISTS books (
    id INT8 DEFAULT nextval('books_id_seq') PRIMARY KEY,
    title VARCHAR NOT NULL,
    isbn VARCHAR(17) NOT NULL CHECK (isbn ~* '^\d{3}-\d{2}-\d{5}-\d{2}-\d$'),
    year SMALLINT NOT NULL,
    author_id INT NOT NULL REFERENCES authors(id),
    annotation TEXT NOT NULL,
    price DECIMAL(2) NOT NULL
);

