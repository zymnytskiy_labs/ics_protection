CREATE ROLE operator LOGIN;

GRANT USAGE, SELECT ON SEQUENCE authors_id_seq, books_id_seq TO operator;
GRANT SELECT, INSERT, UPDATE ON authors, books TO operator;

