CREATE SEQUENCE IF NOT EXISTS authors_id_seq;

CREATE TABLE IF NOT EXISTS authors (
    id INT8 DEFAULT nextval('authors_id_seq') PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL
);

