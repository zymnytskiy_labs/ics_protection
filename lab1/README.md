# Dependencies

* Linux Kernel 3.10 or higher (required by Docker)
* Docker
* GNU Make

# Running tests

* Make sure that your Docker daemon is running
* Build docker image

```
make docker_build
```

* Run image in container

```
make docker_run_daemon
```

* Run tests (by the way, PostgreSQL needs some time for initializing and binding to the port)

```
make check
```

