BEGIN;

SELECT plan(15);

SELECT has_table('books');

SELECT columns_are('books', ARRAY['id', 'title', 'isbn', 'year', 'author_id', 'annotation', 'price']);
SELECT col_not_null('books', 'id');
SELECT col_not_null('books', 'title');
SELECT col_not_null('books', 'isbn');
SELECT col_not_null('books', 'year');
SELECT col_not_null('books', 'author_id');
SELECT col_not_null('books', 'annotation');
SELECT col_not_null('books', 'price');

SELECT col_is_pk('books', 'id');

SELECT fk_ok('books', 'author_id', 'authors', 'id');

CREATE FUNCTION books_insert(text, bigint) RETURNS void AS $$
    INSERT INTO books (title, isbn, year, author_id, annotation, price)
    VALUES ('some title', $1, 2017, $2, 'some annotation', 20.00);
$$ LANGUAGE sql;

-- check isbn format
SELECT throws_ok($$ SELECT books_insert('bad isbn', 0) $$, 'new row for relation "books" violates check constraint "books_isbn_check"');
SELECT throws_ok($$ SELECT books_insert('0000000000000', 0) $$, 'new row for relation "books" violates check constraint "books_isbn_check"');

-- check author_id foreign key
SELECT throws_ok($$ SELECT books_insert('000-00-00000-00-0', 0) $$, 'insert or update on table "books" violates foreign key constraint "books_author_id_fkey"');
INSERT INTO authors (first_name, last_name) VALUES ('Stephen', 'King');
SELECT lives_ok($$ SELECT books_insert('000-00-00000-00-0'::text, (SELECT id FROM authors LIMIT 1)) $$);

SELECT finish();

ROLLBACK;

