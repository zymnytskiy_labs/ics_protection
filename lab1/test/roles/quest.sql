BEGIN;

SELECT plan(2);

SELECT throws_matching($$ SELECT * FROM authors; $$, 'permission denied');
SELECT throws_matching($$ SELECT * FROM books; $$, 'permission denied');

SELECT finish();

ROLLBACK;

