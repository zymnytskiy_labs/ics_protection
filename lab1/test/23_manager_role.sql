BEGIN;

SELECT plan(3);

SELECT has_role('manager');

SELECT table_privs_are('authors', 'manager', ARRAY['SELECT']);
SELECT table_privs_are('books', 'manager', ARRAY['SELECT']);

SELECT finish();

ROLLBACK;

