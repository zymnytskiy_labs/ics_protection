BEGIN;

SELECT plan(4);

SELECT has_role('guest');

SELECT table_privs_are('books_info', 'guest', ARRAY['SELECT']);
SELECT table_privs_are('authors', 'guest', ARRAY[]::text[]);
SELECT table_privs_are('books', 'guest', ARRAY[]::text[]);

SELECT finish();

ROLLBACK;

