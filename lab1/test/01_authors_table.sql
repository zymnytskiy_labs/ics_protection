BEGIN;

SELECT plan(7);

SELECT has_table('authors');
SELECT columns_are('authors', ARRAY['id', 'first_name', 'last_name']);
SELECT col_not_null('authors', 'id');
SELECT col_not_null('authors', 'first_name');
SELECT col_not_null('authors', 'last_name');

SELECT col_is_pk('authors', 'id');

INSERT INTO authors (first_name, last_name) VALUES ('George', 'Byron'), ('Mary', 'Shelley');
SELECT ok((SELECT COUNT(*) FROM authors) = 2, 'has inserted');

SELECT finish();

ROLLBACK;

