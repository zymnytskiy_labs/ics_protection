BEGIN;

SELECT plan(3);

SELECT has_role('operator');

SELECT table_privs_are('authors', 'operator', ARRAY['SELECT', 'INSERT', 'UPDATE']);
SELECT table_privs_are('books', 'operator', ARRAY['SELECT', 'INSERT', 'UPDATE']);

SELECT finish();

ROLLBACK;

