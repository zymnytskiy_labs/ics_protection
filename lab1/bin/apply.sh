#!/bin/bash

BIN_ROOT=$(realpath $(dirname $0))
PROJECT_ROOT=$(realpath $(dirname $0)/..)

for file in $PROJECT_ROOT/sql/*.sql; do
	$BIN_ROOT/psql_wrapper < $file
done

